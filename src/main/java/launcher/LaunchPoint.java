package launcher;

public @interface LaunchPoint {
	public String id();
}
