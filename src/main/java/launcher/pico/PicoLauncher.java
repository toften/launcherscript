package launcher.pico;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.lang.reflect.Method;

import org.picocontainer.PicoContainer;
import org.picocontainer.containers.CommandLinePicoContainer;
import org.picocontainer.script.xml.XMLContainerBuilder;

public class PicoLauncher {
    private static class LauncherParameters {
        private String className;
        private String methodName;
        private String parameterClassName;

        public LauncherParameters(String className, String methodName, String parameterClassName) {
            this.className = className;
            this.methodName = methodName;
            this.parameterClassName = parameterClassName;
        }
        
        public String toString() {
            return className + "/" + methodName + "/" + parameterClassName;
        }
    }

    private static final String PICO_SCRIPT = "pico.script";
    
    private static PicoContainer commandLineContainer;
    private PicoContainer runContainer;
    
    public static void main(String[] args) throws Exception {
        commandLineContainer = new CommandLinePicoContainer(args);
        
        new PicoLauncher().launch(commandLineContainer.getComponent(LauncherParameters.class));
    }

    private void launch(LauncherParameters p) throws Exception {
        String fileName = System.getProperty(PICO_SCRIPT);
        Reader script = new FileReader(new File(fileName));
System.out.println(p.toString());        
        XMLContainerBuilder b = new XMLContainerBuilder(script, this.getClass().getClassLoader());
        
        runContainer = b.buildContainer(commandLineContainer, null, true);
        
        Class<?> pClass = Class.forName(p.className);
        Object runO = runContainer.getComponent(pClass);
        
        if (p.methodName != null) {
            if (p.parameterClassName != null) {
                Class<?> paramClass = Class.forName(p.parameterClassName);
                Object param = commandLineContainer.getComponent(paramClass);
                
                Method m = pClass.getMethod(p.methodName, paramClass);
                m.invoke(runO, param);
            } else {
                Method m = pClass.getMethod(p.methodName);
                m.invoke(runO);
            }
        }
    }
}