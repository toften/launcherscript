package launcher;

public @interface LaunchParameter {
	String[] name();
}
