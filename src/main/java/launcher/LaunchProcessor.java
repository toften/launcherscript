package launcher;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import javax.tools.JavaCompiler.CompilationTask;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import launcher.config.App;
import launcher.config.Launcher;
import launcher.config.ObjectFactory;
import net.toften.processor.AbstractProcessorMojo;
import net.toften.processor.PropertyConsumer;

@SupportedSourceVersion(SourceVersion.RELEASE_6)
@SupportedAnnotationTypes("launcher.LaunchPoint")
public class LaunchProcessor
    extends
        AbstractProcessorMojo
    implements
        Processor,
        PropertyConsumer {
    private JAXBContext jc;

    public LaunchProcessor() {
        try {
            jc = JAXBContext.newInstance("launcher.config", this.getClass().getClassLoader());
        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public boolean process(Set<? extends TypeElement> arg0, RoundEnvironment arg1) {
        ObjectFactory of = new ObjectFactory();

        // Get LaunchPoint annotated methods
        Set<? extends Element> lps = arg1.getElementsAnnotatedWith(LaunchPoint.class);

        if (lps.size() > 0) {
            App app = of.createApp();
            app.setLaunchers(of.createAppLaunchers());

            for (Element launchMethod : lps) {
                LaunchPoint lp = launchMethod.getAnnotation(LaunchPoint.class);

                Launcher l = of.createLauncher();
                l.setClazz(launchMethod.getEnclosingElement().toString());
                l.setMethod(launchMethod.toString());
                l.setId(lp.id());

                app.getLaunchers().getLauncher().add(l);
            }

            try {
                Marshaller m = jc.createMarshaller();
                m.marshal(app, pw);
            } catch (Exception e) {

            }
        }

        return false;
    }

    public static void main(String[] args) throws IOException {
        List<String> files = new ArrayList<String>();
        files.add("launcher.AppWithLauncher");
        File[] files1 = { new File("src/test/java/launcher/AppWithLauncher.java") }; // input for first compilation task
        // File[] files2 = ... ; // input for second compilation task

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

        StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);

        Iterable<? extends JavaFileObject> compilationUnits1 = fileManager.getJavaFileObjectsFromFiles(Arrays.asList(files1));
        //CompilationTask ct = compiler.getTask(null, fileManager, null, null, null, compilationUnits1);
        CompilationTask ct = compiler.getTask(null, fileManager, null, null, files, compilationUnits1);
        ct.setProcessors(Collections.singletonList(new LaunchProcessor()));
        
        ct.call();

        // Iterable<? extends JavaFileObject> compilationUnits2 =
        // fileManager.getJavaFileObjects(files2); // use alternative method
        // // reuse the same file manager to allow caching of jar files
        // compiler.getTask(null, fileManager, null, null, null, compilationUnits2).call();

        fileManager.close();
    }
}